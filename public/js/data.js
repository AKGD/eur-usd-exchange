var exchangeRates = [
    {
      date: "2018-01-01",
      rate: 1.1845,
      denomination: "USD"
    },
    {
      date: "2018-02-01",
      rate: 1.2352,
      denomination: "USD"
    },
    {
      date: "2018-03-01",
      rate: 1.2312,
      denomination: "USD"
    }, 
    {
      date: "2018-04-01",
      rate: 1.2286,
      denomination: "USD"
    }, 
    {
      date: "2018-05-01",
      rate: 1.2388,
      denomination: "USD"
    },
    {
      date: "2017-01-01",
      rate: 1.0421,
      denomination: "USD"
    },
    {
      date: "2017-02-01",
      rate: 1.0664,
      denomination: "USD"
    },
    {
      date: "2017-03-01",
      rate: 1.0555,
      denomination: "USD"
    },
    {
      date: "2017-04-01",
      rate: 1.0807,
      denomination: "USD"
    },
    {
      date: "2017-05-01",
      rate: 1.0725,
      denomination: "USD"
    },
    {
      date: "2017-06-01",
      rate: 1.1193,
      denomination: "USD"
    },
    {
      date: "2017-07-01",
      rate: 1.1147,
      denomination: "USD"
    },
    {
      date: "2017-08-01",
      rate: 1.1533,
      denomination: "USD"
    },
    {
      date: "2017-09-01",
      rate: 1.1799,
      denomination: "USD"
    },
    {
      date: "2017-10-01",
      rate: 1.2007,
      denomination: "USD"
    },
    {
      date: "2017-11-01",
      rate: 1.1749,
      denomination: "USD"
    },
    {
      date: "2017-12-01",
      rate: 1.1749,
      denomination: "USD"
    },
    {
      date: "2016-01-01",
      rate: 1.0952,
      denomination: "USD"
    },
    {
      date: "2016-02-01",
      rate: 1.0907,
      denomination: "USD"
    },
    {
      date: "2016-03-01",
      rate: 1.1136,
      denomination: "USD"
    },
    {
      date: "2016-04-01",
      rate: 1.1171,
      denomination: "USD"
    },
    {
      date: "2016-05-01",
      rate: 1.1379,
      denomination: "USD"
    },
    {
      date: "2016-06-01",
      rate: 1.1279,
      denomination: "USD"
    },
    {
      date: "2016-07-01",
      rate: 1.1283,
      denomination: "USD"
    },
    {
      date: "2016-08-01",
      rate: 1.1013,
      denomination: "USD"
    },
    {
      date: "2016-09-01",
      rate: 1.1268,
      denomination: "USD"
    },
    {
      date: "2016-10-01",
      rate: 1.115,
      denomination: "USD"
    },
    {
      date: "2016-11-01",
      rate: 1.0979,
      denomination: "USD"
    },
    {
      date: "2016-12-01",
      rate: 1.0602,
      denomination: "USD"
    }
  ];
  
  